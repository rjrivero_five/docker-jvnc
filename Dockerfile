# Add-hoc dockerfile for running spoon PDI

# Usar elJava de Oracle
FROM isuper/java-oracle:latest

# Instalar las dependencias necesarias para conectarse por VNC
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    lxde-core lxterminal vnc4server libswt-gtk-3-jni git && \
    apt-get clean

ENV USER  vnc
ENV GROUP vnc

# Añadir usuario "vnc", para no tener peleas con los permisos
RUN groupadd -g 1000 vnc && \
    useradd  -g 1000 -m -u 1000 -d /home/vnc -s /bin/bash vnc && \
    mkdir /home/vnc/.vnc

# Descargar noVNC
RUN git clone https://github.com/kanaka/noVNC.git /opt/noVNC && \
    chown -R vnc:vnc /opt/noVNC && \
    mv /opt/noVNC/vnc_auto.html /opt/noVNC/index.html

# Montar el directorio de PDI aqui
VOLUME /home/vnc/Documents

# Puertos Websockify
EXPOSE 6080

# Ejecutar VNC como tarea principal del contenedor
ADD  files/vnc.sh /opt/vnc.sh
ADD  files/xstartup /home/vnc/.vnc/xstartup
CMD  /opt/vnc.sh

