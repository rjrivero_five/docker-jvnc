Java + VNC Container
====================

This container includes an Oracle Java development environment, and an
VNC server for remote connectivity, providing a LXDE Desktop.

The VNC server is started in desktop :1, so the container exposes **Port 5901**.

To build the container:

```
git clone https://bitbucket.org/five_corp/docker-jvnc.git
cd docker-jvnc

docker build -t jvnc:master .
```

To run:

```
docker run --rm -p 5901:5901 \
  -v /opt/jvnc:/home/vnc/Documents \
  --name jvnc jvnc:master
```

Environment variables
---------------------

The container does not use any environment variable.

Volumes
-------

The container mounts volume **/home/vnc/Documents**, and changes the ownership
of every file below that point to user **vnc:vnc** (uid 1000, gid 1000), which
happen to be the user and group ids of the first user in Ubuntu 14.04.

VNC Password
------------

In case there is a file **vncpassword** in */home/vnc/Documents*, the container
uses it to bootstrap the VNC server password. VNC passwords must be **between
6 and 8 characters long**.

Otherwise, it creates a random password and stores it in the file. The password
is also dumped to the docker logs.

Screen Resolution
-----------------

The VNC server is started at 800x600, but it supports xrandr, so it can be
resized using **xrandr**: *xrandr -s 1920x1080*.

Several resolutions are
available. They can be listed using the commjand **xrandr** without arguments.
