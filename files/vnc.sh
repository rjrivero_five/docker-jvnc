#!/usr/bin/env bash

# USER env variable is provided by Dockerfile to be "vnc"
export HOME="/home/$USER"

# Si no existe un password VNC, lo creamos
if  [ ! -f "$HOME/.vnc/passwd" ]; then
  mkdir -p "$HOME/.vnc"

  # El password se intenta leer de $HOME/Documents/vncpasswd.
  # Los passwords VNC deben tener entre 6 y 8 caracteres.
  if  [ -f "$HOME/Documents/vncpasswd" ]; then
    export PASS=`cat "$HOME/Documents/vncpasswd"`

  # Si el fichero no existe, se crea un pass aleatorio y se guarda ahi.
  else
    export PASS=`openssl rand -base64 6`
    echo "$PASS" > "$HOME/Documents/vncpasswd"
  fi

  # Ejecutamos vncpasswd para "cifrar" la clave
  echo "** VNC PASSWORD: ** '$PASS'"
  echo -e "${PASS}\n${PASS}" | vncpasswd
fi

# Cambiamos la propiedad del directorio al usuario VNC
chown -R "$USER:$GROUP" "$HOME"
chmod 0600 "$HOME/.vnc/passwd"
cd "$HOME"

# Ejecutamos VNC
su - "$USER" -c "USER=$USER vncserver :1 -depth 24 \
 -geometry 800x600   \
 -geometry 1920x1080 \
 -geometry 1280x1024 \
 -geometry 1600x900  \
 -geometry 1440x900  \
 -geometry 1280x800  \
 -geometry 1280x720  \
 -geometry 1024x768  \
 -geometry 800x600"

# Lanzamos el portal web. Puerto por defecto: 6080
su - "$USER" -c "/opt/noVNC/utils/launch.sh --vnc localhost:5901"
